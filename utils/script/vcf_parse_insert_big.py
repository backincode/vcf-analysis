#!/usr/bin/env python


import time
import cql
# import urllib

# output filename
# outputName = time.strftime("%Y%m%d")
# outputName += '.txt'

# ATTENTION CHECK CORRECT PATH:
# path of vcf file
VCF_PATH = "/mnt/data/vcf_big.vcf"
# path of malattie file
DISEASES_PATH = "/mnt/data/db_malattie.tsv"
# path of locazione_sesso file
LOCATION_SEX_PATH = "/mnt/data/locazione_sesso.panel"
# path of parentela file
PARENTELA_PATH = "/mnt/data/parentela.ped"

# = urllib.URL)

vcfHeader = []

# FORMAT:  GT:DP:GL
# map: patient->[CROM, ID, REF, ALT, FORMAT, TODO]
patients2data = {}

rs2diseases = {}

patients2locationsex = {}

patients2parentela = {}


listaOutput = []

def readParentelaFile() :
	print("START - readParentelaFile: " +time.strftime("%Y/%m/%d %H-%M-%S"))
	for line in open(PARENTELA_PATH) :
		values = line.strip("\n").split("\t")
		tempMap = { "paternal-id" : values[2], "maternal-id" : values[3], "siblings" : values[8], "children" : values[11] }
		patient = values[1]
		patients2parentela[patient] = tempMap
	print("END - readParentelaFile: " +time.strftime("%Y/%m/%d %H-%M-%S"))

# save into patientslocationsex {HG00001=>[disease1, disease2, disease3]}
def readLocationSexFile() :
    print("START - readLocationSexFile: " +time.strftime("%Y/%m/%d %H-%M-%S"))
    for line in open(LOCATION_SEX_PATH) :
		# param 1, to split in two array
		#values = line.split("\t", 1)
		values = line.strip("\n").split("\t")
		patient = values[0]
		locationSex = values[1]+ "|" +values[2]+ "|" +values[3]
		patients2locationsex[patient] = locationSex
    print("END - readLocationSexFile: " +time.strftime("%Y/%m/%d %H-%M-%S"))
    return patients2locationsex

# save into rs2diseases {RS->malattia, RS->malattia}
def readDiseasesFile() :
    print( "START - readDiseasesFile: " +time.strftime("%Y/%m/%d %H-%M-%S"))
    for line in open(DISEASES_PATH) :
		value = line.split("\t")
		RS = value[1]
		disease = value[0]
		if disease is None :
			disease = 'None'
		# check if already exists a key with this RS
		if RS in rs2diseases :
			rs2diseases[RS].append(disease)
		else :
			rs2diseases[RS] = [disease]
    print( "END - readDiseasesFile: " +time.strftime("%Y/%m/%d %H-%M-%S"))
    return rs2diseases

# @filename: "/percorso/completo/nome-del-file.vcf"
# @return: an array of patients
def readVCFPatients() :
    print( "START - readVCFPatients: " +time.strftime("%Y/%m/%d %H-%M-%S"))
    for line in open(VCF_PATH) :
		# when parser founds header, saves them in array
		if line.startswith("#CHROM") :
			header = line.strip("\n|#").split("\t")
			for value in header :
				vcfHeader.append(value)
			print("END - readVCFPatients: " +time.strftime("%Y/%m/%d %H-%M-%S"))
			return vcfHeader


# initiate patients2data with key:patient, value:none
# eg.: patients2data = {HG00001:None, HG00002:None, HG00003: None, etc}
def populatePatients2data() :
	patients = vcfHeader[9:]			
	patients2data.update(dict.fromkeys(patients, []))
	return patients2data


def getVariant(REF, ALT) :
	variant = ""
	if REF != ALT :
		variant += 'SNP'
	if len(REF) > len(ALT) :
		variant += '+DELETION'
	if len(REF) < len(ALT) :
		variant += '+INSERTION'
	return variant

def insertIntoPatients2data() :
    print( "START - insertIntoPatients2data: " +time.strftime("%Y/%m/%d %H-%M-%S"))
    for line in open(VCF_PATH) :
		# if line is not header, start to save data into map
		if line[0:1] != "#" :
			values = line.strip("\n|#").split("\t")
			CHROM = values[0]
			ID = values[2]
			REF = values[3]
			ALT = values[4]
			variant = getVariant(REF, ALT)
			# eg: GT:DP:GL
			FORMAT = values[8]
			FORMAT_fields = FORMAT.split(":")
		
			patients = values[9:]

			i=0
			# iterate over each patients column
			for patientValue in patients :
				patientKey = patients2data.keys()[i]

				# match FORMAT_fields[i] with atientValue_values[i]
				patientValue_values = patientValue.split(":")
				GT = patientValue_values[0]
				DP = patientValue_values[1]
				if patientValue_values[2] != '.' :
					GLvalues = patientValue_values[2].split(",")
					GL1 = GLvalues[0]
					GL2 = GLvalues[1]
				else:
					GL1 = '.'
					GL2 = '.'

				disease = rs2diseases.get(ID)
				locationSex = patients2locationsex.get(patientKey)
				# eg: pat_geo, pat_region, pat_gender
				locationSex_values = locationSex.split("|");
				pat_geo = locationSex_values[0]
				pat_region = locationSex_values[1]
				pat_gender = locationSex_values[2]


				
				# retrieve values because patients2parentela is a map of map
				parentela = patients2parentela.get(patientKey);
				paternal_id = parentela.get("paternal-id")
				maternal_id = parentela.get("maternal-id")
				siblings = parentela.get("siblings")
				children = parentela.get("children")
				
				tempValuesList = [CHROM, ID, disease, REF, ALT, variant, patientKey, pat_geo, pat_region, paternal_id, maternal_id, siblings, children, pat_gender, DP, GL1, GL2, GT ]
				
				tempFinal= []

				for it in tempValuesList:
					if it is None:
						tempFinal.append("Not mapped on db")
					else:
						if it is '.':
							tempFinal.append("Unknow varitation")
						else:
							tempFinal.append(it)

				listaOutput.append(tempFinal)
				i += 1
    print( "END - insertIntoPatients2data: " +time.strftime("%Y/%m/%d %H-%M-%S"))
    return patients2data

# visual test.
def printData4Cassandra() :
	with open("cassandra.txt", 'w') as output :
		i = 0
		for lista in listaOutput :
			if i > 8000 :
				return
			output.write( str(lista) +"\n" )
			i += 1

def insertIntoCassandra() :
    print( "START - insertIntoCassandra: " +time.strftime("%Y/%m/%d %H-%M-%S"))
    conn = cql.connect('127.0.0.1', '9160', 'vcfkeyspace', cql_version="3.0.0")
    cursor = conn.cursor()
    for lista in listaOutput :
    	cursor.execute('INSERT INTO vcfoperations (chrom , rs_code , rs_disease , var_rel , var_alt , var_type ,pat_code , pat_geo , pat_region , pat_mom, pat_dad, pat_sibling, pat_child , pat_gender , val_dp , val_gl_1, val_gl_2 , val_gt)  VALUES (:chrom , :rs_code , :rs_disease , :var_rel , :var_alt , :var_type ,:pat_code , :pat_geo , :pat_region , :pat_mom, :pat_dad, :pat_sibling, :pat_child , :pat_gender , :val_dp , :val_gl_1 , :val_gl_2, :val_gt);', {'chrom':lista[0], 'rs_code':lista[1], 'rs_disease':lista[2], 'var_rel':lista[3], 'var_alt':lista[4], 'var_type':lista[5], 'pat_code':lista[6], 'pat_geo':lista[7], 'pat_region':lista[8], 'pat_mom':lista[9], 'pat_dad':lista[10], 'pat_sibling':lista[11], 'pat_child':lista[12], 'pat_gender':lista[13], 'val_dp':lista[14], 'val_gl_1':lista[15], 'val_gl_2':lista[16] , 'val_gt':lista[17]})
    	cursor.execute('INSERT INTO vcfnations (rs_code, rs_disease, val_gl_2, val_gt, val_dp, pat_code, pat_geo, pat_region) VALUES (:rs_code, :rs_disease, :val_gl_2, :val_gt, :val_dp, :pat_code, :pat_geo, :pat_region);', {'rs_code':lista[1], 'rs_disease':lista[2], 'val_gl_2':lista[16], 'val_gt':lista[17], 'val_dp':lista[14], 'pat_code':lista[6], 'pat_geo':lista[7], 'pat_region':lista[8]})
    	cursor.execute('INSERT INTO vcfrelatives (rs_code, rs_disease, pat_code, pat_mom, pat_dad, pat_sibling, pat_child, pat_geo, val_gl_2, val_gt) VALUES (:rs_code, :rs_disease, :pat_code, :pat_mom, :pat_dad, :pat_sibling, :pat_child, :pat_geo, :val_gl_2, :val_gt);', {'rs_code':lista[1], 'rs_disease':lista[2], 'pat_code':lista[6], 'pat_mom':lista[9], 'pat_dad':lista[10], 'pat_sibling':lista[11], 'pat_child':lista[12], 'pat_geo':lista[7], 'val_gl_2':lista[16], 'val_gt':lista[17]})
    print( "END - insertIntoCassandra: " +time.strftime("%Y/%m/%d %H-%M-%S"))


# call functions.
readVCFPatients()
readDiseasesFile()
readLocationSexFile()
readParentelaFile()
populatePatients2data()
insertIntoPatients2data()
#printData4Cassandra()
insertIntoCassandra()