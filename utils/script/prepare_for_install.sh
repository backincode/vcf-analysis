#!/bin/bash

# sciprt for prepare Cassandra installation and setup.
echo
echo "###################### PREPARE INSTALL ######################"
echo
echo "...get data:"
echo
echo "take install_cassandra script ..."
echo

# get install_cassandra script.
wget https://s3.amazonaws.com/backincode/script/install_cassandra.sh

# change permission.
sudo chmod +x install_cassandra.sh

echo
echo "take jar ..."
echo
# get jar for execution.
wget https://s3.amazonaws.com/backincode/script/simple-client-cassandra-0.0.1-SNAPSHOT.jar

# change permission.
sudo chmod +x simple-client-cassandra-0.0.1-SNAPSHOT.jar

echo
echo "take parser ..."
echo

# get parser.
wget https://s3.amazonaws.com/backincode/script/vcf_parse_insert.py

# get parser for complete vcf.
wget https://s3.amazonaws.com/backincode/script/vcf_parse_insert_big.py

# change permission.
sudo chmod +x vcf_parse_insert.py
sudo chmdo +x vcf_parse_insert_big.py

echo
echo "take commands ..."
echo

# get commands.
wget https://s3.amazonaws.com/backincode/script/commands.cql

# change permission.
sudo chmod +x commands.cql

echo
echo "take runner ..."
echo

# get runner.
wget https://s3.amazonaws.com/backincode/script/vcf_analysis.sh
wget https://s3.amazonaws.com/backincode/script/vcf_analysis_big.sh

# change permission.
sudo chmod +x vcf_analysis.sh
sudo chmod +x vcf_analysis_big.sh

echo
echo "take data ..."
echo

# get db_malattie.tsv
wget https://s3.amazonaws.com/backincode/input/data/db_malattie.tsv

# move it.
sudo mkdir /mnt/data
sudo mv db_malattie.tsv /mnt/data

# get locazione_sesso.panel
wget https://s3.amazonaws.com/backincode/input/data/locazione_sesso.panel

# move it.
sudo mv locazione_sesso.panel /mnt/data

# get parentela.ped
wget https://s3.amazonaws.com/backincode/input/data/parentela.ped

# move it.
sudo mv parentela.ped /mnt/data

# get vcf
wget https://s3.amazonaws.com/backincode/input/data/vcf_sample.vcf

# move it.
sudo mv vcf_sample.vcf /mnt/data

# get vcf complete
wget ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/supporting/vcf_with_sample_level_annotation/ALL.chrY.phase3_integrated_extra_anno.20130502.genotypes.vcf.gz

# extract data
gunzip ALL.chrY.phase3_integrated_extra_anno.20130502.genotypes.vcf.gz

# rename it
mv ALL.chrY.phase3_integrated_extra_anno.20130502.genotypes.vcf vcf_big.vcf

# move it
sudo mv vcf_big.vcf /mnt/data

echo
echo "create tables folder ..."
echo
# tables folder.
sudo mkdir /mnt/tables

echo
echo "take spark ..."
echo

# get spark.
wget https://s3.amazonaws.com/backincode/spark/spark-1.4.0.tgz

# change permission.
sudo chmod +x spark-1.4.0.tgz

# unpack spark.
sudo tar xvf spark-1.4.0.tgz

# move spark in opt.
sudo mv spark-1.4.0 /opt

echo
echo "export credential ..."
echo

# export aws credential.
export AWS_ACCESS_KEY_ID=AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=AWS_SECRET_ACCESS_KEY

echo
echo "install python library ..."
echo

# instal cql library.
sudo pip install cql

echo
echo "###################### LAUNCH INSTALL CASSANDRA SCRIPT ######################"
echo

# launch install_cassandra script.
sudo ./install_cassandra.sh