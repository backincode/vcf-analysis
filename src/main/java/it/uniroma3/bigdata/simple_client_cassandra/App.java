package it.uniroma3.bigdata.simple_client_cassandra;

import java.util.Iterator;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;

public class App {

	private Cluster cluster;

	   public void connect(String node) {
	      cluster = Cluster.builder()
	            .addContactPoint(node).build();
	      Metadata metadata = cluster.getMetadata();
	      System.out.printf("Connected to cluster: %s\n", 
	            metadata.getClusterName());
	      
	      Iterator<Host> it = metadata.getAllHosts().iterator();
	      while(it.hasNext()){
	    	  Host host = it.next();
	    	  System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n",
		               host.getDatacenter(), host.getAddress(), host.getRack());}
	      
	   } 

	   public void close() {
		  cluster.close();
	      
	   }

	   public static void main(String[] args) {
	      App client = new App();
	      client.connect("127.0.0.1");
	      client.close();
	   }
	

}
