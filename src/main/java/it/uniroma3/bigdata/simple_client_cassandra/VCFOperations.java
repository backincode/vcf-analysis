package it.uniroma3.bigdata.simple_client_cassandra;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class VCFOperations {
	private static Properties properties;
	private PreparedStatement ps_insert_all = null;
	private PreparedStatement ps_insert_all_nat = null;
	private PreparedStatement ps_insert_all_rel = null;
	private PreparedStatement ps_select_all = null;
	private PreparedStatement ps_select_all_nat = null;
	private PreparedStatement ps_select_all_rel = null;
	private PreparedStatement ps_table = null;
	private PreparedStatement ps_vcfnations = null;
	private PreparedStatement ps_vcfrelatives = null;
	Cluster cluster = null;
	Session session;

	/**
	 * Disconnect from the current cluster
	 */
	public void disconnect() {
		this.session.close();
		this.cluster.close();
		System.out.println("Disconnected!!");
	}
	/**
	 * @param ip
	 * Connected to the keyspace and node
	 */
	public void connect(String ip) {
		this.cluster = Cluster.builder()
				.addContactPoints(ip)
				.build();
		this.session = cluster.connect();
		System.out.println("Connected!!");
	}

	/**
	 * @param propertiesFileName
	 * @throws IOException 
	 */
	private static void loadProperties(InputStream propStream, Properties prop) throws IOException {
		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		propStream = contextClassLoader.getResourceAsStream("queries.properties");

		if(propStream != null){
			prop.load(propStream);
		} else {
			System.out.println("Sorry, unable to find properties file!");
			return;
		}

	}

	/* ========== PREPARE SECTION: ========== */

	/**
	 * 
	 * create keyspace: 
	 */
	public void createKeySpace(){
		this.session.execute(properties.getProperty("CREATE_KS"));

	}

	/**
	 * 
	 * create table: 
	 * 
	 */
	public void createColumnFamily() {
		BoundStatement boundStatement = null;
		BoundStatement boundVcfnations = null;
		BoundStatement boundVcfrelatives = null;

		if(this.ps_table == null)
			this.ps_table = this.session.prepare(properties.getProperty("CREATE_CF"));
		if(this.ps_vcfnations == null)
			this.ps_vcfnations = this.session.prepare(properties.getProperty("CREATE_CF_NATION"));
		if(this.ps_vcfrelatives == null)
			this.ps_vcfrelatives = this.session.prepare(properties.getProperty("CREATE_CF_RELATIVES"));

		boundStatement = new BoundStatement(this.ps_table);
		boundVcfnations = new BoundStatement(this.ps_vcfnations);
		boundVcfrelatives = new BoundStatement(this.ps_vcfrelatives);

		this.session.execute(boundStatement);
		this.session.execute(boundVcfnations);
		this.session.execute(boundVcfrelatives);

	}

	/*
	 * create indexes:
	 * 
	 */
	public void createIndexes(){
		this.session.execute(properties.getProperty("CREATE_INDEX_GENDER"));
		this.session.execute(properties.getProperty("CREATE_INDEX_GEO"));
		this.session.execute(properties.getProperty("CREATE_INDEX_NAT_GL_2"));
		this.session.execute(properties.getProperty("CREATE_INDEX_NAT_GT"));
		this.session.execute(properties.getProperty("CREATE_INDEX_REL_GL_2"));
		this.session.execute(properties.getProperty("CREATE_INDEX_REL_GT"));
	}

	/* ========== QUERIES SECTION: ========== */

	/*
	 * method for prepare all statement:
	 */
	public void prepareAllStatement(){
		if(this.ps_select_all   == null)
			this.ps_select_all = this.session.prepare(properties.getProperty("SELECT_ALL"));
		if(this.ps_select_all_nat   == null)
			this.ps_select_all_nat = this.session.prepare(properties.getProperty("SELECT_ALL_NAT"));
		if(this.ps_select_all_rel   == null)
			this.ps_select_all_rel = this.session.prepare(properties.getProperty("SELECT_ALL_REL"));
		if(this.ps_insert_all   == null)
			this.ps_insert_all = this.session.prepare(properties.getProperty("INSERT_ALL"));
		if(this.ps_insert_all_nat   == null)
			this.ps_insert_all_nat = this.session.prepare(properties.getProperty("INSERT_ALL_NAT"));
		if(this.ps_insert_all_rel   == null)
			this.ps_insert_all_rel = this.session.prepare(properties.getProperty("INSERT_ALL_REL"));

	}

	/**
	 * @param rs_code
	 * @param pat_code
	 * insert the data to column family
	 * SUPPORT METHOD FOR TEST.
	 * 
	 */
	public void insertAll(String chrom, String rs_code, String rs_disease, String var_rel, 
			String var_alt, String var_type, String pat_code, String pat_geo, String pat_region,
			String pat_mom, String pat_dad, String pat_sibling, String pat_child, String pat_gender, 
			String val_dp, String val_gl_1, String val_gl_2, String val_gt) {
		BoundStatement boundStatement = null;

		if(this.ps_insert_all != null){
			boundStatement = new BoundStatement(this.ps_insert_all);
			this.session.execute(boundStatement.bind(chrom, rs_code, rs_disease, var_rel, 
					var_alt, var_type, pat_code, pat_geo, pat_region,
					pat_mom, pat_dad, pat_sibling, pat_child, pat_gender, 
					val_dp, val_gl_1, val_gl_2, val_gt));
		}
	}

	/**
	 * @param rs_code
	 * @param pat_code
	 * insert the data to column family
	 * SUPPORT METHOD FOR TEST.
	 * 
	 */
	public void insertAllNat(String rs_code, String rs_disease, String val_gl_2, String val_gt, 
			String val_dp, String pat_code, String pat_geo, String pat_region) {
		BoundStatement boundStatement = null;

		if(this.ps_insert_all_nat != null){
			boundStatement = new BoundStatement(this.ps_insert_all_nat);
			this.session.execute(boundStatement.bind(rs_code, rs_disease, val_gl_2, val_gt, 
					val_dp, pat_code, pat_geo, pat_region));
		}
	}

	/**
	 * @param rs_code
	 * @param pat_code
	 * insert the data to column family
	 * SUPPORT METHOD FOR TEST.
	 * 
	 */
	public void insertAllRel(String rs_code, String rs_disease, String pat_code, String pat_mom, 
			String pat_dad, String pat_sibling, String pat_child, String pat_geo, String val_gl_2, String val_gt){
		BoundStatement boundStatement = null;

		if(this.ps_insert_all_rel != null){
			boundStatement = new BoundStatement(this.ps_insert_all_rel);
			this.session.execute(boundStatement.bind(rs_code, rs_disease, pat_code, pat_mom, pat_dad, 
					pat_sibling, pat_child, pat_geo,val_gl_2, val_gt));
		}
	}

	/*
	 * 
	 * accessory method for insert all.
	 * SUPPORT METHOD FOR TEST.
	 * */
	public static void insertAllEntries(VCFOperations object){
		for(int i=0; i<50000; i++){
			object.insertAll("Y","rs11575"+i+"897" ,"Cancer","G" ,"A" ,"INDEL" , 
					"HG"+i+"0096" ,"GBR" , "EUR","HG00412" ,"HG01541" ,"HG00412" ,"HG01541", "M" , 
					"-1.1" ,"0.3" ,"2.1" ,"3"); // insert_all.
			System.err.println("Inserted "); // check.
			object.insertAll("X","rs1"+i+"573594" ,"Word reading","T" ,"A" ,"INDEL" , 
					"HG0"+i+"123" ,"GWD" ,"EUR","HG00412" ,"HG01541" ,"HG00412" ,"HG01541", "F" , 
					"1.6" ,"-0.3" ,"-1.1" ,"-3.02"); // insert_all.
			System.err.println("Inserted "); // check.
			object.insertAll("3","rs125758"+i+"7" ,"AIDS progression","C" ,"A" ,"DEL" , 
					"HG001"+i+"3" ,"FIN" ,"EUR","HG00148" ,"HG00151" ,"HG00412" ,"HG01541", "M" , 
					"1.1" ,"-0.8", "-3" ,"5.3"); // insert_all.
			System.err.println("Inserted "); // check.
			object.insertAll("3","rs1257587"+i ,"AIDS progression","C" ,"A" ,"DEL" , 
					"HG0"+i+"103" ,"FIN" ,"EUR","HG00148" ,"HG00151" ,"HG00412" ,"HG01541", "M" , 
					"1.1" ,"-0.8" , "3","-2"); // insert_all.
			System.err.println("Inserted "); // check.

			object.insertAllNat("rs11575"+i+"897" ,"Cancer", "2.1" , "3" , "-1.1" , "HG"+i+"0096" ,  "GBR" , "EUR"); // insert_all.
			System.err.println("Inserted "); // check.
			object.insertAllNat("rs1"+i+"573594" ,"Word reading","-3.02" , "-1.1" , "1.6" , "HG0"+i+"123" ,"GWD" ,"EUR"); // insert_all.
			System.err.println("Inserted "); // check.

			object.insertAllRel("rs11575"+i+"897" ,"Cancer", "HG"+i+"0096" , "HG00412" ,"HG01541" ,"HG00412" ,"HG01541", "GBR" , "2.1" , "3"); // insert_all.
			System.err.println("Inserted "); // check.

		}

	}

	/**
	 * 
	 * Select all the rows from the given columnfamily
	 * SUPPORT METHOD FOR TEST.
	 */
	public void selectAll() {
		BoundStatement boundStatement = null;

		if(this.ps_select_all != null){
			boundStatement = new BoundStatement(this.ps_select_all);
			ResultSet rs = this.session.execute(boundStatement);
			for (Row row : rs) {
				System.out.println(row.toString());
			}
		}
	}

	/**
	 * 
	 * Select all the rows from the given columnfamily
	 * SUPPORT METHOD FOR TEST.
	 */
	public void selectAllNat() {
		BoundStatement boundStatement = null;

		if(this.ps_select_all_nat != null){
			boundStatement = new BoundStatement(this.ps_select_all_nat);
			ResultSet rs = this.session.execute(boundStatement);
			for (Row row : rs) {
				System.out.println(row.toString());
			}
		}
	}

	/**
	 * 
	 * Select all the rows from the given columnfamily
	 * SUPPORT METHOD FOR TEST.
	 */
	public void selectAllRel() {
		BoundStatement boundStatement = null;

		if(this.ps_select_all_rel != null){
			boundStatement = new BoundStatement(this.ps_select_all_rel);
			ResultSet rs = this.session.execute(boundStatement);
			for (Row row : rs) {
				System.out.println(row.toString());
			}
		}
	}

	/* ========== MAIN SECTION: ========== */

	/**
	 * 
	 * main method: 
	 */
	public static void main(String[] args) throws IOException {

		long start = System.currentTimeMillis();
		System.out.println(start);

		VCFOperations object = new VCFOperations();	
		properties = new Properties();
		InputStream propertiesStream = null;

		// loading properties:
		loadProperties(propertiesStream,properties);

		/*	====	OPERATIONS	====	*/
		object.connect(properties.getProperty("SERVER_IP")); // connect to cassandra.
		object.createKeySpace(); // create vcfkeyspace.
		object.createColumnFamily(); // create vcfoperations.
		object.createIndexes(); // indexes for columns: pat_geo, pat_gender.
		object.prepareAllStatement(); // prepare queries.

		/* ========== CLOSE CONNECTION. ========== */
		object.disconnect(); // disconnect from cassandra.

		long end =System.currentTimeMillis();
		System.out.println(end);
		long microseconds = (end - start) / 1000;
		System.out.println(microseconds);

	}

}