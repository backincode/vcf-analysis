#!/bin/bash

# sciprt for install Cassandra.
echo
echo "###################### INSTALL CASSANDRA ######################"
echo "... installig update!"
echo

# installing update:
sudo yum update

echo
echo "update: DONE"
echo "... installing scala!"
echo
# remove installed version of scala in ec2:
sudo yum remove scala

# take scala 2.10:
wget http://www.scala-lang.org/files/archive/scala-2.10.0.rpm

# install scala 2.10:
sudo yum install scala-2.10.rpm

echo
echo "scala: DONE"
echo "...take cassandra from s3 bucket!"
echo

# take cassandra from s3
wget https://s3.amazonaws.com/backincode/cassandra/apache-cassandra-2.1.7-bin.tar.gz
echo
echo "... install cassandra!"
echo

# unpack
tar -xzvf apache-cassandra-2.1.7-bin.tar.gz
# move in /opt
sudo mv apache-cassandra-2.1.7 /opt

# creating cassandra default directory:
sudo mkdir -p /var/lib/cassandra/data	# data
sudo mkdir -p /var/lib/cassandra/commitlog	# commitlog
sudo mkdir -p /var/lib/cassandra/saved_caches	#saved_caches
sudo chown -R ec2-user.ec2-user /var/lib/cassandra # permission

# cassandra log directory:
sudo mkdir -p /var/log/cassandra	# log
sudo chown -R ec2-user.ec2-user /var/log/cassandra	# permission

echo
echo "cassandra: DONE"
echo

# hadoop path:
export HADOOP_CLASSPATH=/opt/apache-cassandra-2.1.7/lib/*:$HADOOP_CLASSPATH

echo
echo "###################### LAUNCH CASSANDRA ######################"
echo

# run cassandra:
sudo /opt/apache-cassandra-2.1.7/bin/cassandra -f
