package it.uniroma3.bigdata.simple_client_cassandra;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;

public class VCFSpark {

	private static final String APP = "VCFOperations";
	private static final String MACHINES = "local[8]";
	private static final String MEMORY = "10g";
	private static final String MAX_RES = "spark.driver.maxResultSize";
	private static final String MAX_RES_MEM = "15g";
	private static final String EXECUTOR = "spark.executor.memory";
	private static final String HOST = "spark.cassandra.connection.host";
	private static final String SERVER = "127.0.0.1";

	public SparkConf initSpark(){
		// Spark Conf:
		SparkConf conf = new SparkConf(true)
		.setAppName(APP)
		.setMaster(MACHINES)
		.set(EXECUTOR, MEMORY)
		.set(MAX_RES, MAX_RES_MEM)
		.set(HOST, SERVER);

		return conf;
	}

	public static void main(String[] args){
		// temp,
		long start = System.currentTimeMillis();
		System.out.println(start);

		// disable log on console.
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);

		// Vars.
		String format = "com.databricks.spark.csv";
		String opt_1 = "header";
		String opt_2 = "true";
		String source = "/mnt/tables/";
		String output = "/mnt/output/";

		// create a VCFSpark:
		VCFSpark vcf_spark = new VCFSpark();
		// get spark conf:
		SparkConf conf = vcf_spark.initSpark();
		// create a java spark context.
		JavaSparkContext context = new JavaSparkContext(conf);
		// create an sql context.
		SQLContext sqlContext = new SQLContext(context);

		// create data frame from csv files.
		DataFrame df_nat = sqlContext.read().format(format).option(opt_1, opt_2).load(source+"nations.csv");
		DataFrame df_rel = sqlContext.read().format(format).option(opt_1, opt_2).load(source+"relatives.csv");
		DataFrame df_op = sqlContext.read().format(format).option(opt_1, opt_2).load(source+"operations.csv");

		// cache it for execute in ram.
		df_nat.cache();
		df_rel.cache();
		df_op.cache();

		/* ========== QUERIES SECTION: ========== */

		// query 1:
		// time_1.
		long time_1_start =System.currentTimeMillis();
		System.out.println(time_1_start);

		DataFrame query_1 = df_nat.filter(df_nat.col("pat_region").equalTo("EUR")).filter(df_nat.col("val_gl_2").equalTo("0")).filter(df_nat.col("val_gt").equalTo("1"));
		query_1.select("rs_code","pat_code","rs_disease","val_dp").write().format(format).save(output+"out_query_1");

		long time_1_end =System.currentTimeMillis();
		System.out.println(time_1_end);
		long ms_1 = (time_1_end - time_1_start) / 1000;
		System.out.println(ms_1);

		// query 2:
		// time_2.
		long time_2_start =System.currentTimeMillis();
		System.out.println(time_2_start);

		DataFrame query_2 = df_nat.filter(df_nat.col("pat_region").equalTo("EUR")).filter(df_nat.col("val_gl_2").notEqual("0")).filter(df_nat.col("val_gt").equalTo("1"));
		query_2.select("rs_code","pat_code","rs_disease","val_dp").write().format(format).save(output+"out_query_2");

		long time_2_end =System.currentTimeMillis();
		System.out.println(time_2_end);
		long ms_2 = (time_2_end - time_2_start) / 1000;
		System.out.println(ms_2);

		// query 3:
		// time_3.
		long time_3_start =System.currentTimeMillis();
		System.out.println(time_3_start);

		DataFrame query_3 = df_nat.filter(df_nat.col("pat_region").equalTo("EUR")).filter(df_nat.col("val_gl_2").equalTo("0")).filter(df_nat.col("val_gt").equalTo("0"));
		query_3.select("rs_code","pat_code","rs_disease","val_dp").write().format(format).save(output+"out_query_3");

		long time_3_end =System.currentTimeMillis();
		System.out.println(time_3_end);
		long ms_3 = (time_3_end - time_3_start) / 1000;
		System.out.println(ms_3);

		// query 4:
		// time_4.
		long time_4_start =System.currentTimeMillis();
		System.out.println(time_4_start);

		DataFrame query_4 = df_rel.filter(df_rel.col("pat_child").notEqual("0"));
		query_4.select("rs_code","pat_code","rs_disease").write().format(format).save(output+"out_query_4");

		long time_4_end =System.currentTimeMillis();
		System.out.println(time_4_end);
		long ms_4 = (time_4_end - time_4_start) / 1000;
		System.out.println(ms_4);

		// query 5:
		// time_5.
		long time_5_start =System.currentTimeMillis();
		System.out.println(time_5_start);

		DataFrame query_5 = df_rel.filter(df_rel.col("pat_sibling").notEqual("0"));
		query_5.select("rs_code","pat_code","rs_disease").write().format(format).save(output+"out_query_5");

		// time_4.
		long time_5_end =System.currentTimeMillis();
		System.out.println(time_5_end);
		long ms_5 = (time_5_end - time_5_start) / 1000;
		System.out.println(ms_5);

		// query 6:
		// time_6.
		long time_6_start =System.currentTimeMillis();
		System.out.println(time_6_start);

		DataFrame query_6_a = df_rel.filter(df_rel.col("val_gl_2").equalTo("0")).filter(df_rel.col("val_gt").equalTo("1")).filter(df_rel.col("pat_child").notEqual("0")).select("pat_child","rs_code");


		DataFrame df_temp = query_6_a.toDF("pat_child1","rs_code1");
		DataFrame query_6_b = df_rel.filter(df_rel.col("val_gl_2").equalTo("0")).filter(df_rel.col("val_gt").equalTo("1")).select("rs_code","pat_code", "rs_disease").join(df_temp,df_temp.col("pat_child1").equalTo(df_rel.col("pat_code")));//.join(query_6_a, query_6_a.col("pat_child").equalTo("pat_code"),"left_outer");

		DataFrame df_temp2 = query_6_b.toDF("rs_code","pat_code","rs_disease","pat_cod_child","rs_cod_child");
		DataFrame query_6_c = df_temp2.join(df_temp,df_temp.col("rs_code1").equalTo(df_temp2.col("rs_code")));

		query_6_c.select("rs_code1","rs_disease","pat_code").write().format(format).save(output+"out_query_6");

		// time_6.
		long time_6_end =System.currentTimeMillis();
		System.out.println(time_6_end);
		long ms_6 = (time_6_end - time_6_start) / 1000;
		System.out.println(ms_6);

		/* ========== QUERIES SECTION: ========== */
		// stop context.
		context.stop();

		// temp end.
		long end =System.currentTimeMillis();
		System.out.println(end);
		long microseconds = (end - start) / 1000;
		System.out.println(microseconds);

	}

}
