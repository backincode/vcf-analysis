# VCF Analysis #

Generate structure, manipulation and analysis for VCF files

### Big Data - Final Project ###

* Università degli studi Roma Tre
* A.A. 2014 - 2015
* Group: BackInCode
* Link source: [ https://bitbucket.org/backincode/vcf-analysis](https://bitbucket.org/backincode/vcf-analysis)

### Description: ###

This project generates a structure for management and manipulation of VCF files.

* Create and initialize an Apache Cassandra DB.
* Parse VCF files stored in Amazon S3 cloud and also can parse file directly from ftp  1000Genomes server.
* Populate DB with parsed and aggregated files.
* Make queries and analysis via Apache Spark and store results.

### Project strucure: ###


```


	+-VCF Analysis
	|
	|------+-src
	|	   |
	|	   +--------+-main
	|	            |       
	|	            |--------+ java/it.uniroma3.bigdata.simple_client_cassandra
	|	            |        |
	|	    	    |        |--------+-App.java	   
	|	    	    |        |
	|	            |        |--------+-VCFOperations.java
	|	    	    |        |
	|	    	    |        |--------+-VCFSpark.java
	|	            |
	|               |--------+-resources
	|	    			     |
	|	    	       		 |--------+-queries.properties
	|	    
	|------+-utils
	       |	
	       +--------+-script
		   |	    |--------+-vcf_analysis.sh
	       |	    |
		   |	    |--------+-vcf_analysis_big.sh
	       |	    |
	       |	    |--------+-vcf_parse_insert.py
		   |	    |
		   |	    |--------+-vcf_parse_insert_big.py
		   |	    |
		   |	    |--------+-commands.cql
		   |        |
		   |        |--------+-prepare_for_install.sh
		   |        |
		   |        |--------+-install_cassandra.sh
		   |
		   |--------+-how_to_run_script.txt
```

        
### How to run: ###

For run application follow the instructions in /VCF Analysis/utils/how_to_run_script.txt file.