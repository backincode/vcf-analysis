#!/bin/bash

# script runner.

echo
echo "###################### VCF ANALYSIS RUNNER ######################"
echo
echo "...ready!"
echo
echo "create structures for cassandra ..."
echo

# launch vcf operations.
java -cp simple-client-cassandra-0.0.1-SNAPSHOT.jar it.uniroma3.bigdata.simple_client_cassandra.VCFOperations

echo
echo "...completed vcf operations!"
echo
echo "parse vcf file and insert data to cassandra ..."
echo

# launch parser.
python vcf_parse_insert_big.py

echo
echo "...completed parser & insert!"
echo
echo "export tables in csv format for sparkql queries ..."
echo

# launch commands.
sudo /opt/apache-cassandra-2.1.7/bin/cqlsh -f /home/hadoop/commands.cql

echo
echo "...completed export tables!"
echo
echo "insert header in csv ..."
echo

# insert headers.
sudo ed -s /mnt/tables/nations.csv < <(printf '%s\n' 1i "rs_code,pat_code,pat_geo,pat_region,rs_disease,val_dp,val_gl_2,val_gt" . wq)
sudo ed -s /mnt/tables/relatives.csv < <(printf '%s\n' 1i "rs_code,pat_code,pat_child,pat_dad,pat_geo,pat_mom,pat_sibling,rs_disease,val_gl_2,val_gt" . wq)
sudo ed -s /mnt/tables/operations.csv < <(printf '%s\n' 1i "rs_code,pat_code,chrom,pat_child,pat_dad,pat_gender,pat_geo,pat_mom,pat_region,pat_sibling,rs_disease,val_dp,val_gl_1,val_gl_2,val_gt,var_alt,var_rel,var_type" . wq)

echo
echo "...completed insert headers!"
echo
echo "spark queries ..."
echo

# launch sparkql queries.
java -Xmx30g -cp simple-client-cassandra-0.0.1-SNAPSHOT.jar it.uniroma3.bigdata.simple_client_cassandra.VCFSpark

echo
echo "...completed spark queries!"
echo
echo "################### END VCF ANALYSIS RUNNER ######################"
echo